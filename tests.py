import logging

from main import convert_time_table_html_to_dataframe, detect_if_sold_out

# TODO: Use an actual unit test framework

def test_convert_time_table_html_to_dataframe():
    with open("time_table-RESERVE_EXAMPLE.html", "r") as f:
        time_table = f.read()
    df = convert_time_table_html_to_dataframe(time_table)
    print(df)

    not_sold_out = (
        df[~df.eq("Sold Out")].dropna(how="all").dropna(axis="columns", how="all")
    )
    print(not_sold_out)

    assert not not_sold_out.empty


def test_detect_if_sold_out():
    logFormatter = logging.Formatter(
        "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s"
    )
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.INFO)
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)

    day_str = "test-day"
    time_str = "test-time"

    with open("time_table-RESERVE_EXAMPLE.html", "r") as f:
        df = convert_time_table_html_to_dataframe(f.read())
    assert detect_if_sold_out(df, day_str, time_str, is_audible=False)

    with open("time_table-SOLD_OUT_EXAMPLE.html", "r") as f:
        df = convert_time_table_html_to_dataframe(f.read())
    assert not detect_if_sold_out(df, day_str, time_str, is_audible=False)


if __name__ == "__main__":
    test_convert_time_table_html_to_dataframe()

    test_detect_if_sold_out()
