# Selenium Web Scrape Bot

This project sets up a Python script to scrape a table of data off of a web page periodically, convert the table to CSV, and alert the user if the table contains certain values.
Specifically, the data is a table of time slots and the detection method checks if the individual slots have openings. If there are open slots, the script make an audible tone.
In this project, we use Selenium to programmatically access the website and scrape the HTML off of the page, BeautifulSoup4 to convert the HTML to a data frame, and Pandas to convert the dataframe to CSV and detect time slot openings.

## I cloned this and followed your instructions to run it but it threw errors. What gives?

I have uploaded this project in order to share my implementation with others who are interested in web scraping projects. I don't want this project copied into an army of bots scraping a poor, innocent website.

Therefore, I have removed all site-specific information from the repository. So if you clone this repo and try to run it as-is, it will not run.

That information is contained in two files that are explicitly ignored by git:
 * `site_data.json`, which contains information about the site urls, class, and XPATHs used to navigate and scrape the data. 
 * `credentials.json`, which contains the account username and password used to log in.

## Pre-requisites

* Windows OS (see the instructions for `get_selenium_drivers.py`)
* Google Chrome (see the instructions for `get_selenium_drivers.py`)
* Python 3.7+ with Pipenv installed:
```
pip install pipenv
```

## Instructions

1. In the root directory of the project, use pipenv to create a virtual environment and enter it:
```bash
$ pipenv install
$ pipenv shell # You can skip this if you prefix all following commands with `pipenv run ...`
```

2. Download the Selenium driver by running:
```
$ python get_selenium_driver.py
```
Note: Currently, this script only downloads a specific version of the Windows Chrome selenium driver. This can be easily modified to switch to another driver. Extending the script to support all the drivers is left as an exercise for the reader 🙂.

3. Run the script to start scraping
```
$ python main.py
```
Or see what the command line parameters are 
```
$ python main.py -h
```

## Sample Output

```
2020-04-22 08:56:33,426 [MainThread  ] [INFO ]  ## Logging in...
2020-04-22 08:56:49,971 [MainThread  ] [INFO ]  ########################################
2020-04-22 08:56:49,977 [MainThread  ] [INFO ]  ## Scraping fresh data @ 2020-04-22_08-56-49...
2020-04-22 08:56:56,259 [MainThread  ] [INFO ]  ## All slots are sold out :(
2020-04-22 08:56:56,260 [MainThread  ] [INFO ]  ########################################
...
2020-04-22 15:01:06,564 [MainThread  ] [INFO ]  ########################################
2020-04-22 15:01:06,913 [MainThread  ] [INFO ]  ## Logging in again...
2020-04-22 15:01:13,376 [MainThread  ] [INFO ]  ## Scraping fresh data @ 2020-04-22_15-01-13...
2020-04-22 15:01:19,685 [MainThread  ] [INFO ]  ## All slots are sold out :(
2020-04-22 15:01:19,686 [MainThread  ] [INFO ]  ########################################
2020-04-22 15:16:21,307 [MainThread  ] [INFO ]  ########################################
2020-04-22 15:16:21,311 [MainThread  ] [INFO ]  ## Scraping fresh data @ 2020-04-22_15-16-21...
2020-04-22 15:16:27,600 [MainThread  ] [INFO ]  ##!!!! SOME SLOTS ARE OPEN !!!!
               3: Sun, Apr 26
8am - 8:30am          Open
8:30am - 9am          Open
10am - 10:30am        Open
11:30am - 12pm        Open
1pm - 1:30pm          Open
2020-04-22 15:16:28,640 [MainThread  ] [INFO ]  ########################################
```
