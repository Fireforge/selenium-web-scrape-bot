import platform
from pathlib import Path
from urllib.parse import urlparse
from urllib.request import urlretrieve
import zipfile

if platform.system() == 'Windows':
    url_str = 'https://chromedriver.storage.googleapis.com/81.0.4044.69/chromedriver_win32.zip'
else:
    raise Exception('Does not support platforms other than Windows yet')
    # TODO support other valid Selenium platforms and browser other than Chrome

# Copy the the path to the file which contains the vesion number to use for our own download path
downloadfile_urlpath = urlparse(url_str).path.strip('/')
driver_basepath = Path.cwd().joinpath('selenium_driver')
driver_archive_path = driver_basepath.joinpath(downloadfile_urlpath)

# check if the file has already been downloaded or if it's out of date
# Note: The version number is embedded in the path, so checking for existance will also catch if the version is new
if driver_archive_path.exists():
    print('Driver up-to-date')
else:
    # download the zip file
    driver_archive_path.parent.mkdir(parents=True, exist_ok=True)
    urlretrieve(url_str, filename=str(driver_archive_path))

    # unzip the file
    with zipfile.ZipFile(str(driver_archive_path), 'r') as file:
        file.extractall(path=str(driver_basepath))
    print('New driver downloaded: ', driver_archive_path)

