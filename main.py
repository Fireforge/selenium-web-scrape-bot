import sys
import time
import logging
import traceback
import json
from pathlib import Path
import argparse

from pprint import pprint as pp

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
import pandas as pd

with open("site_data.json", "r") as f:
    site_data = json.load(f)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--rate",
        type=int,
        help="The time, in seconds, between individual page checks. Defaults to five minutes.",
        default=5 * 60,
    )
    parser.add_argument(
        "--test-beep",
        action="store_true",
        help="Produce a test beep when the program starts. Useful to ensure the sound configuration works as intended.",
    )
    return parser.parse_args()

def init_logging():
    """Setup logging to support both console and log file output"""
    logFormatter = logging.Formatter(
        "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s"
    )
    rootLogger = logging.getLogger()
    rootLogger.setLevel(logging.DEBUG)

    fileHandler = logging.FileHandler("info.log")
    fileHandler.setLevel(logging.INFO)
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    fileHandler = logging.FileHandler("debug.log")
    fileHandler.setLevel(logging.DEBUG)
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setLevel(logging.INFO)
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)


def beep():
    """Produce a loud alert tone."""
    import winsound

    duration = 1000  # milliseconds
    freq = 440  # Hz
    winsound.Beep(freq, duration)


### Selenium => HTML of time slots
CHROME_DRIVER_PATH = Path.cwd().joinpath("selenium_driver/chromedriver.exe")
WEBDRIVERWAIT_TIMEOUT = 120


def throttle():
    """Waits a number of seconds to keep the script from proceeding through the 
    pages at super-human speeds. This should be called after each WebDriverWait returns."""
    time.sleep(2.25)


def get_to_login(driver):
    """Given a fresh driver, goes to the sign in page."""
    driver.get(site_data["url_home"])

    element = WebDriverWait(driver, WEBDRIVERWAIT_TIMEOUT).until(
        EC.presence_of_element_located((By.ID, site_data["elem_signin_btn"]))
    )
    throttle()

    driver.get(element.get_attribute("href"))


def sign_in(driver, cred):
    """Enter the given sign-in credentials into the page, then proceed to the timeslots page."""
    WebDriverWait(driver, WEBDRIVERWAIT_TIMEOUT).until(
        EC.presence_of_element_located((By.ID, "Email"))
    )
    throttle()

    driver.find_element(By.ID, "Email").send_keys(cred["Email"])
    driver.find_element(By.ID, "Password").send_keys(cred["Password"])
    driver.find_element(By.ID, "SignIn").click()
    throttle()

    # Check that the signin has taken straight to the timeslots page. If so, we're done.
    if driver.title == site_data["title_timeslots"]:
        return

    # Grab the link to the timeslot page and navigate there
    WebDriverWait(driver, WEBDRIVERWAIT_TIMEOUT).until(
        EC.presence_of_element_located((By.ID, site_data["elem_timeslots_link"]))
    )
    throttle()

    timeslot_link = driver.find_element(By.ID, site_data["elem_timeslots_link"]).get_attribute("href")
    driver.get(timeslot_link)

    WebDriverWait(driver, WEBDRIVERWAIT_TIMEOUT).until(
        EC.title_is(site_data["title_timeslots"])
    )
    throttle()


def get_time_table_html(driver):
    """When on the timeslot page, extract the HTML that contains the timeslot table."""
    WebDriverWait(driver, WEBDRIVERWAIT_TIMEOUT).until(
        EC.presence_of_element_located((By.XPATH, site_data["xpath_sendquery"]))
    )
    throttle()

    # Click the element that sends the request to load the timeslot data into the page
    driver.find_element(By.XPATH, site_data["xpath_sendquery"]).click()
    WebDriverWait(driver, WEBDRIVERWAIT_TIMEOUT).until(
        EC.presence_of_element_located((By.XPATH, site_data["xpath_timeslotparent"]))
    )
    throttle()

    # Get the timeslot data out of the parent div which contains it
    time_table = driver.find_element(By.XPATH, site_data["xpath_timeslotparent"]).get_attribute(
        "innerHTML"
    )
    return time_table


### BeautifulSoup => Pandas
def convert_time_table_html_to_dataframe(time_table: str):
    """Given the timeslot table HTML segment, convert it into a Pandas DataFrame."""
    soup = BeautifulSoup(time_table, "html.parser")

    # Get the descriptions of the times from the table
    times = soup.find_all("div", attrs={"class": site_data["class_time"]})
    times = [div.span.contents[0] for div in times]

    # Collect all of the information about the days and the slots available on each day
    day_dict = {}
    for i, day in enumerate(
        soup.find_all("div", attrs={"class": site_data["class_day"]})
    ):
        # Get the parent div with the class that marks the Days
        # TODO: use a single "find" instead of "find_all"?
        day_info = day.find_all("div", attrs={"class": site_data["class_dayinfo"]})
        # just take the parent div and get all the children which contain the labels for each day
        day_info = day_info[0].find_all("div")
        # extract the day's descripton and re-format it so it looks good in the table
        day_info = f"{i}: " + ", ".join(div.contents[0].strip() for div in day_info)

        # Get the divs with the class that marks the slots for the current day
        day_slots = day.find_all("div", attrs={"class": site_data["class_slotinfo"]})

        # Extract the text describing the state of the each slot
        def extract_slot_button_text(button):
            """Extract text from the correct element given the button structure"""
            if button.has_attr(site_data["attr_soldout"]):  # "Sold Out" case
                return button.text.strip()
            # else if: # "Reserve (1 SLOT LEFT)" case?
            else:  # "Reserve" case
                return button.contents[3].text

        day_slots = tuple(extract_slot_button_text(div.button) for div in day_slots)

        # Add the slot info for this day into the dict
        day_dict[day_info] = day_slots

    # Convert the dict into a DataFrame
    df = pd.DataFrame.from_records(data=day_dict, index=times)
    return df


def detect_if_sold_out(df, day_str, time_str, is_audible=True):
    not_sold_out = (
        df[~df.eq("Sold Out")].dropna(how="all").dropna(axis="columns", how="all")
    )

    if not_sold_out.empty:
        logging.info("## All slots are sold out :(")
    else:
        logging.info("##!!!! SOME SLOTS ARE OPEN !!!!\n" + str(not_sold_out))
        # Sound a beep alert to notify the user that slots are open
        if is_audible:
            beep()
        # Save the details of which slots are open for analysis
        result_csv_path = Path(f"not_sold_out/{day_str}/{time_str}.csv")
        result_csv_path.parent.mkdir(parents=True, exist_ok=True)
        not_sold_out.to_csv(result_csv_path)
    return not not_sold_out.empty


def main():
    args = parse_args()
    init_logging()

    # Initial beep to test to make sure the sound is functional
    if args.test_beep:
        beep() 

    # TODO: !!SECURITY!! Figure out how to use encrypted credentials. No plain-text!
    with open("credentials.json", "r") as f:
        cred = json.load(f)

    logging.info("Starting main process")

    # Perform the inital log-in through Selenium
    driver = webdriver.Chrome(
        CHROME_DRIVER_PATH
    )  # Optional argument, if not specified will search path.
    try:
        logging.info(f"## Logging in...")
        get_to_login(driver)
        sign_in(driver, cred)
    except Exception as exc:
        logging.error(traceback.format_exc())
        logging.error(exc)
        driver.quit()

    while True:
        succeeded = True
        try:
            time_table = None

            logging.info("#" * 40)
            # Check to see if the session expired, and break if it has to retrigger a login
            if driver.current_url == site_data["url_signin"]:
                logging.info(f"## Logging in again...")
                sign_in(driver, cred)

            # TODO - replace all this timestamp stuff with a dataclass
            day_str = time.strftime(r"%Y-%m-%d")
            time_str = time.strftime(r"%H-%M-%S")
            timestamp_str = f"{day_str}_{time_str}"
            logging.info(f"## Scraping fresh data @ {timestamp_str}...")

            time_table = get_time_table_html(driver)
            with open("time_table.html", "w") as f:
                f.write(time_table)
            # with open('time_table.html', 'r') as f:
            #     time_table = f.read()

            df = convert_time_table_html_to_dataframe(time_table)

            detect_if_sold_out(df, day_str, time_str)
            # TODO: dynamic rate change
            #   Increase: when a first detection is made, automatically bump the sample rate up
            #   Decrease: after we see a good number of "all sold out"s, bump the rate back down to normal levels

            logging.info("#" * 40)
        except Exception as exc:
            logging.error(traceback.format_exc())
            logging.error(exc)
            if time_table != None:
                with open(f"ERROR-time_table-{timestamp_str}.html", "w") as f:
                    f.write(time_table)
            succeeded = False

        if succeeded:
            # TODO: change system to work with scheduled times (with some fuzziness) rather than dumb wait timers
            time.sleep(args.rate)
        else:
            # If the process fails, wait five seconds and try again
            # TODO: Limit the number of retries before we kick back out to a re-login?
            time.sleep(5)

        driver.refresh()

    time.sleep(
        60
    )  # Waiting to allow time to inspect the page that Selenium ended up on
    driver.quit()
    logging.info("Exiting main process")


if __name__ == "__main__":
    main()
